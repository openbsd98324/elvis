<html>
<head>
<title>Elvis 2.1 How To</title>
</head>
<body>
<h1>C. How to...</h1>
This is a collection of "How to" topics, each with short discussion.
The following catagories of topics are available:
<ul>
<li><a href="#Initialization">Initialization</a>
- Change the default behavior of elvis
<li><a href="#WordCompletion">Word Completion</a>
- Alter word completion via the <kbd>Tab</kbd> key
<li><a href="#Whitespace">Whitespace</a>
- The relationship between tabs and spaces
<li><a href="#Buffers">Buffers</a>
- Cut buffers and edit buffers
<li><a href="#X-Windows">X-Windows</a>
- The "x11" user interface
<li><a href="#Windows95/98/NT">Windows95/98/NT</a>
- The "windows" user interface
<li><a href="#Miscellany">Miscellany</a>
- Things that didn't fit anywhere else
<li><a href="#Contacts">Contacts</a>
- How to contact the authors
</ul>

<p>If you're using elvis to view this file, you can search for a topic
simply by using the <a href="elvisvi.html#slash">/</a> command.
To limit the search to topic lines only, begin the regular expression
with "^&lt;dt&gt;.*".
For example you could search for "spaces" in a topic line via the following:

<pre>
	/^&lt;dt&gt;.*spaces</pre>

<p>Sometimes there are multiple topics that use the same word or phrase.
If the first one that it finds isn't the one you want, then you can use
the <a href="elvisvi.html#n">n</a> command to find the next one.

<p>In addition, the <code>lib/elvis.ali</code> script distributed with elvis
contains a "howto" alias which loads this file and searches for a given topic.
The aliases from that file are loaded automatically, so
you should be able to display any topic in a
a separate window via a command such as this:

<pre>
	:howto spaces</pre>

<h2>C.1 <a name="Initialization">Initialization</a></h2>
<dl>

<dt>Initialization <!-- initialization initialize --> for each file
<dd>The <a href="elvisses.html#elvis.arf">lib/elvis.arf</a> file is executed
after a file has been read;
you can put file initialization commands there.

<dt>Make file initialization be language-sensitive
<dd>If the filename extension indicates the file's language, and the language
is described in the <a href="elvisdm.html#elvis.syn">lib/elvis.syn</a> file,
then you can use the <code>knownsyntax()</code> function to check the
language, like this:

<pre>
	:if knownsyntax(filename) == "perl"
	:then set ccprg="perl -c ($1?$1:$2) 2&gt;&amp;1 | perlerr"</pre>

<p>(The <code>perlerr</code> perl script converts perl's error messages
into a form that elvis can parse.
It is given in the <a href="elvistip.html#perlerr">Tips</a> chapter.)

<p>You could also use the <code>dirext()</code> function instead of
<code>knownsyntax()</code>; it doesn't depend on the
<a href="elvisdm.html#elvis.syn">lib/elvis.syn</a> file.
As a last resort, you might consider using the <code>x</code> flag of
the <a href="elvisex.html#substitute">:s</a> command.

<dt>Change <!-- change set --> the default <!-- initial startup --> settings <!-- configuration colors options macros -->
<dd>Before the first buffer is loaded or the first window is created,
and buffer/window options you set will be used as the defaults for
the buffers/windows that are created later.
In other words, although setting a buffer-specific option like 
<a href="elvisopt.html#equalprg">equalprg</a> or a window-specific option
like <a href="elvisopt.html#ruler">ruler</a> interactively
will only affect that buffer or window, setting the same option in the
<a href="elvisses.html#elvis.ini">lib/elvis.ini</a> or <code>~/.exrc</code>
file changes the default for all buffers/windows.

<p>However, the <a href="elvisses.html#elvis.brf">lib/elvis.brf</a> and
<a href="elvisses.html#elvis.arf">lib/elvis.arf</a> files typically
change some of those options, so the defaults might not be used very long
before they're changed.

<dt>Have a separate ~/.exrc <!-- elvis.rc --> and ~/.elvisrc file under Unix
<dd>This is easy.
The <a href="elvisses.html#elvis.ini">lib/elvis.ini</a> file controls this,
and the default version does it in a very convenient way:
It looks first for <code>.elvisrc</code> and runs it if it exists;
else it looks for <code>.exrc</code> and runs that.

<p>If you want elvis to run both of them, then add the following line
to your <code>.elvisrc</code> file:

<pre>
	:source ~/.exrc</pre>
</dl>

<h2>C.2 <a name="WordCompletion">Word Completion</a></h2>
<dl>
<dt>Disable <!-- complete --> word <!-- name --> completion on the ex command line
<dd>You need to set the <a href="elvisopt.html#inputtab">inputtab</a> option
for the <code>(Elvis&nbsp;ex&nbsp;history)</code> buffer to "tab".
You can do that via the following command:

<pre>
	:(Elvis ex history)set inputtab=tab</pre>

<dt>Disable <!-- complete --> identifier <!-- name --> completion on the regular expression prompt line
<dd>This is similar to word completion on the ex command line.
To disable it, you need to set the <a href="elvisopt.html#inputtab">inputtab</a>
option for the <code>(Elvis&nbsp;regexp&nbsp;history)</code> buffer, like this:

<pre>
	:(Elvis regexp history)set inputtab=tab</pre>

<dt>Enable <!-- complete --> identifier <!-- name --> completion in normal edit buffers
<dd>Set the buffer's <a href="elvisopt.html#inputtab">inputtab</a> option
to "identifier".
To make this the default, set it in the <code>.exrc</code> file
(or <code>elvis.rc</code> for non-Unix systems).
The command is:

<pre>
	:set inputtab=identifier</pre>

<p>After that, each time you hit the &lt;<kbd>Tab</kbd>&gt; key elvis will
search through the tags file for any matching tags,
and add as many characters as possible.
If it completes the entire tag name, it does <em>not</em> append a space or
other character, which is a little different from other types of name
completion.
Also, in the <a href="elvisdm.html#syntax">syntax</a> display mode it will
not attempt completion if the partial word happens to be a complete keyword
or otherword.
</dl>

<h2>C.3 <a name="Whitespace">Whitespace</a></h2>
<dl>

<dt>Use <!--space tab tabs--> spaces instead of tabs<!--tab-->
<dd>In the traditional vi, the only way to make elvis use spaces instead of
tabs was to set the <a href="elvisopt.html#tabstop">tabstop</a> option to
the largest possible value, and then <a href="elvisex.html#map">:map</a> ^I 
to ^T.
This made existing tabbed files look bad, and it didn't work well for tabs
embedded in a line (instead of just in the line's indentation whitespace).
Elvis has a better way.

<p>In elvis, you can set the <a href="elvisopt.html#inputtab">inputtab</a>
option to "spaces" to make the <kbd>&lt;Tab&gt;</kbd> key insert the
appropriate number of spaces into a line.
This works even if the cursor isn't in the line's indentation whitespace.

<p>In addition, the <a href="elvisopt.html#autotab">autotab</a> option
controls the shifting commands (the <a href="elvisex.html#LT">:&lt;</a> and
<a href="elvisex.html#GT">:&gt;</a> commands in ex, and the
<a href="elvisvi.html#lt">&lt;</a> and <a href="elvisvi.html#gt">&gt;</a>
operators in visual mode).
To make those commands use only spaces, <em>autotab</em> should be off.

<pre>
	:set inputtab=spaces noautotab</pre>

<p>To convert existing files to use only spaces, you should use an external
program such as "<code>col&nbsp;-bx</code>" under Unix.

<dt>Change <!-- change reduce --> the tabstops<!--tabstop tab stop indentation shiftwidth shiftwidth-->
<dd>Many people don't like the fact that the <kbd>&lt;Tab&gt;</kbd> key
indents text by 8 columns.
That's so wide that it quickly pushes the writer's source code off the
right edge of the screen.

<p>However, you almost certainly do <em>not</em> want to change the
<a href="elvisopt.html#tabstop">tabstop</a> option because most other
software, and most printers and terminals, also assume that tabs are 8
characters wide.
If you edit files with <code>tabstop</code> set to 4 or 5, then your
files will look very strange when viewed with anything other than elvis,
or by anyone other than you.
So leave <code>tabstop=8</code>.

<p>Instead, set the <a href="elvisex.html#shiftwidth">shiftwidth</a>
option to the desired indentation amount, and either get in the habit
of typing ^T to increase indentation, or <a href="elvisex.html#map">:map!</a>
the <kbd>&lt;Tab&gt;</kbd> key to ^T in input mode.

<pre>
	:set shiftwidth=5
	:map! ^V^I ^V^T</pre>

<p>Note that when you're typing in the above <code>:map</code> command,
you'll need to type an extra <kbd>^V</kbd> before each <kbd>^V</kbd>
or <kbd>^T</kbd>.

<p>Also, this map has the unfortunate side-effect of making the
<kbd>&lt;Tab&gt;</kbd> increase indentation even if the cursor is somewhere
later in the line (unless you type ^V before it).
This is one good reason to skip the map, and get in the habit of using
<kbd>^T</kbd> to increase indentation.
The <a href="elvisopt.html#autotab">autotab</a> option helps here, too.
</dl>

<h2>C.4 <a name="Buffers">Buffers</a></h2>
<dl>
<dt>Switch <!-- switch goto --> to a different buffer in the same window
<dd>This is easier that you might think.
On an ex command line, if you give an address but no command then elvis
will move the cursor to there.
So to switch buffers all you need to do is give an address that's in
a different buffer.
In elvis, you do this by giving the buffer's name (or number) in parentheses
(and the closing parenthesis is optional).
For example, to switch to buffer #1 all you need to do is...

<pre>
	:(1</pre>

<p>Or you can switch to "main.c" like this:

<pre>
	:(main.c</pre>

<p>Of course, the buffer must exist before you can switch to it.
Another thing to keep in mind is, switching buffers doesn't necessarily
force you to save the old buffer first.
Any changes you made to the old buffer are <em>not</em> lost -- you can
switch back to the original buffer again if you wish.

<dt>Display <!-- show --> an edit buffer (or cut buffer) in a separate window
<dd>This is similar to switching edit buffers (the previous topic).
The main difference is that instead of giving no command, you should give
the <a href="elvisex.html#split">:split</a> command.
In this context, the closing parenthesis is <em>required</em>.

<pre>
	:(1)split</pre>

<p>Or, create a window showing the "main.c" buffer:

<pre>
	:(main.c)split</pre>

<dt>Edit a cut buffer
<dd>Editing a cut buffer can be handy when you're trying to fix a
defective macro.
This is possible in elvis, because elvis uses an ordinary edit buffer to
store the contents of a cut buffer.
The names of the cut buffers are of the form
<code>(Elvis cut buffer <var>X</var>)</code>, where <var>X</var> is the name
of the cut buffer (a single letter or digit).
Consequently, you could create a window showing cut buffer <code>"a</code>
like this:

<pre>
	:(Elvis cut buffer a)split</pre>

<p>Of course, the <code>"a</code> cut buffer must exist for this to work.

<p>Since the name is so long, elvis supports a special short-hand notation
for cut buffer names.
In parentheses, if the first character is " and the remainder of the buffer
name is a single letter, then elvis uses the buffer which contains that
cut buffer's contents.
The following command also creates a window showing the <code>"a</code>
cut buffer:

<pre>
	:("a)sp</pre>

<p>Elvis doesn't store "undo" versions for cut buffers, and
you can't yank a buffer into itself.
Other than that, editing should be pretty normal.
The type of data in the buffer (characters, lines, or rectangle) is
stored in an option named <a href="elvisopt.html#putstyle">putstyle</a>.

<dt>Free <!-- free delete discard --> an edit buffer
<dd>Elvis has no command for discarding old edit buffers.
Under some circumstances it will free them automatically, if they aren't 
being used.
It rarely matters, though.
</dl>

<h2>C.5 <a name="X-Windows">X-Windows</a></h2>
<dl>
<dt>Run <!--run--> elvis in an xterm instead of creating a new window
<dd>You can force elvis to use the termcap interface by adding a
<code>-Gtermcap</code> flag.
If you do this often, you may wish to create a shell script, alias, or shell
function which runs elvis with <code>-Gtermcap</code>.
Here's an example of shell script:
<pre>
	#!/bin/sh
	exec elvis -Gtermcap "$@"</pre>

<p>If you <em>never</em> want to use the "x11" user interface, then you should
probably reconfigure elvis to leave it it out.
This will make elvis considerably smaller.
To do this, go into the directory where elvis' source code resides and
execute the following shell commands:
<pre>
	make clean
	configure --with-x=no
	make</pre>

<dt>Make the text <!--off non-blinking--> cursor more visible under X11<!-- x11 -->
<dd>For a notebook computer, the normal blinking cursor may be hard to see.
You can make it stop blinking by adding the following command to your
<code>.exrc</code> file:

<pre>
	:set blinktime=0</pre>

<dt>Indicate when elvis owns <!-- has posesses --> the current X11<!-- x11 --> selection
<dd>The cursor can be configured to have a special color whenever elvis
owns the current selection.
To do this, use the <a href="elvisex.html#color">:color</a> command to
set both the foreground and background color of the cursor.
The "background" color will be used when elvis owns the selection, and
the "foreground" color will be used otherwise.
Here's an example which turns the cursor green when elvis owns the
selection:

<pre>
	:color cursor red on green</pre>

<dt>Change <!-- change reduce smaller different --> the default font size <!-- X11 x11 -->
<dd>In your <code>.exrc</code> file, you can set the
<a href="elvisopt.html#normalfont">normalfont</a>,
<a href="elvisopt.html#boldfont">boldfont</a>, and
<a href="elvisopt.html#italicfont">italicfont</a> options to anything you want.
These settings will override the defaults.
If you set only the <code>normalfont</code> and leave the others unset,
then elvis will derive the others from the normal font.
<pre>
	:set normalfont=7x14</pre>

<p>If you just want to use a smaller size of the Courier font, you can use
the <code>:courier</code> alias.
It takes a single parameter: the point size of the font to use.
The default font is 18-point Courier, and most systems also have 14-point
Courier fonts which works well.
<pre>
	:courier 14</pre>

<dt>Set<!--set change define use--> a <!--variable-->font for the toolbar buttons.
<dd>By default, the X11 toolbar uses a font named "variable" which is usually
an alias to a small, readable, variable-pitch font.
If your X server has no font named "variable" then you must configure elvis
to use a different font.
You may also simply prefer a different font.

<p>The simplest way to change the font is to pass elvis a
<code>-fc</code>&nbsp;<var>fontname</var> parameter.
I suggest you use that to experiment with the available fonts, to find
one you like.
(You can use the standard <code>xlsfonts</code> program to list the available
fonts.)

<p>To make the change permanent, you can either set the
<a href="elvisopt.html#controlfont">controlfont</a> option in your ~/.exrc file,
or you can set the <a href="elvisgui.html#x11.resources">elvis.control.font</a>
resource in your ~/.Xdefaults file.

<dt>Run<!--run execute fork spawn--> a program<!--process--> from within elvis, in parallel, under X11
<dd>The tricky part here is that elvis tries to read the program's stdout
and stderr, so the output can be displayed in elvis' window.
To do that, Elvis would have to wait until after all text has been read
from stdout/stderr... but you don't want elvis to wait!
So to run in parallel, you must redirect the program's stdout/stderr to
/dev/null, like this:
<pre>
	:!xeyes &gt;/dev/null 2&gt;&amp;1 &amp;</pre>

<p>If you want to write data out to the program (<code>:w !program</code>)
then it becomes even more complex.
This is because pipes can only contain a finite amount of data, so when
elvis is redirecting stdin as well as stdout/stderr, it uses a temporary
file for stdin.
Elvis deletes that file as soon as the program returns -- which, for a program
run in parallel, happens immediately even though the program hasn't had a chance
to read the data from that file yet.
The solution is to write the data into a temporary file sequentially, and
then start a parallel command line which runs the program and then deletes
the temporary file, like this:
<pre>
	:w !cat &gt;$$; (xv $$; rm $$) &gt;/dev/null 2&gt;&amp;1 &amp;</pre>

<p>Yes, that's nasty.
I plan to clean that up some day, by making elvis smart enough to avoid
reading stdout/stderr when the command line ends with a '&amp;' character.

<dt>Run<!--run fork execute spawn--> an interactive program under X11
<dd>Sadly, this can't be done with elvis' "x11" user interface
because elvis' windows don't have a built-in terminal emulator.
This is expected to be added for elvis 2.2, but that doesn't help you
<em>now</em>.

<p>However, by using the "termcap" interface inside an xterm,
you should be able to run interactive programs such as "crypt" or "pgpv"
exactly as you can under vi.
In an xterm (or any other terminal emulator), just run
"<code>elvis&nbsp;-Gtermcap</code>" instead of plain "<code>elvis</code>".

</dl>

<h2>C.6 <a name="Windows95/98/NT">Windows95/98/NT</a></h2>
<dl>
<dt>Set<!--set change define--> the initial<!--current--> working directory
<dd>Create a shortcut to WinElvis, and then edit the shortcut's properties.
The "Start in" property gives the program's initial working directory.

<dt>Change<!--set change define--> the <!--current--> working directory each time a file is loaded
<dd>First, let me say that I don't recommend this because the real vi doesn't
behave like this, so it is likely to confuse some people.

<p>But it you really want to do this, then you should add the following line
to the end of your <a href="elvisses.html#elvis.arf">lib/elvis.arf</a> file:
<pre>
	try cd (dirdir(filename))</pre>

<dt>Select<!--select mark highlight--> whole lines in WinElvis<!--windows winelvis-->
<dd>To select whole lines in WinElvis, move the mouse pointer to the window's
left margin.
The mouse pointer should change shape when you're in the margin.
Click the left mouse button there to begin marking lines;
hold the button as you move the mouse to the other end of the range of lines
and then release it.

<dt>Change<!--change use a different--> the icon for WinElvis<!--winelvis elvis under windows win32-->
<dd>WinElvis contains two icons -- a low-color version of the Elvis Presley
postage stamp, and a tiny photo of elvis.
The stamp is closer to the Unix icon for elvis, so that's the default.
If you prefer the photo, you can use it by creating a link to WinElvis,
viewing the link's properties, and clicking the "Change Icon" button there.
</dl>

<h2>C.7 <a name="Miscellany">Miscellany</a></h2>
<dl>
<dt>Test <!-- test check look --> for certain text within a file
<dd>This is sometimes handy in scripts and aliases.
For example, the <a href="elvisses.html#elvis.arf">elvis.arf</a> script
uses this to detect mode lines and the "hash pling" header on other
types of scripts, such as shell scripts which start with
"<code>#!/bin/sh</code>".

<p>One nice trick is to use the <code>x</code> flag of elvis'
<a href="elvisex.html#substitute">:s</a> command.
It not only detects text, but can incorporate that text into the commands.
For example, to compute the total of all numbers in all lines, you could...

<pre>
	:set t=0
	:%s/\&lt;[[:digit:]]\+\&gt;/let t = t + &amp;/gx
	:set t?</pre>

<p>Note that this series of commands does not affect the edit buffer.
The <code>x</code> flag prevents the substitution from taking place;
the replacement text is executed instead.

<p>You can also use the <a href="elvisex.html#try">:try</a> command
to run a search command, and then use <a href="elvisex.html#then">:then</a>
and <a href="elvisex.html#else">:else</a> to act differently depending on
whether the search succeeded or not.

<pre>
	:try /Yow!
	:then echo Zippy was here
	:else echo Where in the world is Zippy the Pinhead?</pre>

<p>You can also use the <code>current("word")</code> and <code>line()</code>
<a href="elvisexp.html#13.3">functions</a> to fetch the word at the cursor
location, or a whole line, respectively.

<pre>
	:let w=current("word")
	:let l=line(1)</pre>

<dt>Find the <!-- option's --> short name (or group name) of an option
<dd>Finding the long name of an option is easy, thanks to
<a href="elvisex.html#Tab">name completion</a>.
To find the short name of an option, or the name of its group, run the
<a href="elvisex.html#set">:set!</a> command (with a <code>!</code>) and
the long option name, followed by a <code>?</code> character.
(For non-Boolean options, the <code>?</code> is optional.)

<pre>
	:set! wrapmargin?</pre>

<p>This will produce output like "win.wm=0", indicating that the short name
is "wm", the group name is "win" (so each window has its own margin), and
the value is 0.

<dt>Recover <!--recover--> files <!--edit buffers changes--> after a crash
<dd>This is described in the <a href="elvisses.html#RECOVER">Sessions</a>
chapter of the manual.
Briefly, run "<code>elvis -r</code>" to start a new elvis process on the
old session file, and then use the <a href="elvisex.html#buffer">:buffer</a>
command to list the buffers.
You can then use other commands to save those buffers; for example, to
save a buffer named "main.c" into a file named "main.c.recovered", you
would give this command:
<pre>
	:(main.c)w&nbsp;main.c.recovered</pre>

<dt>Recognize <!--recognize parse handle understand read perl--> error messages <!--errors-->with an unusual<!--perl weird other different--> format <!--style-->
<dd>Elvis' <a href="elvisex.html#make">:make</a> and
<a href="elvisex.html#cc">:cc</a> commands assume that all error messages have
a format that resembles that of <strong>gcc</strong>:
A file name and line number appear at the beginning of the line (possibly with
some punctuation or the word "line" mixed in), an optional "error" or "warning"
code, and then the description of the error to finish the line.  Elvis is
very good at parsing messages that use formats which resemble this, but
there is no explicit way to make elvis parse any other format.

<p>However, it is usually possible to construct a little "filter" program to
convert other error message formats into one that elvis can recognize.
The <a href="elvistip.html">Tips chapter</a> has an example of how to make
elvis handle <a href="elvistip.html#perlerr">PERL's error messages</a>.

<dt>Input<!--input type enter key--> non-ASCII<!--non-ascii accented foreign international--> characters<!--letters-->
<dd>Elvis supports several ways to enter non-ASCII characters:
<ul>
<li>If your keyboard driver supports entry of non-ASCII characters, then that
method should work in elvis.
However, "dead keys" have been reported to cause trouble.
<li>Digraphs combine two ASCII characters to form a single non-ASCII character.
See the <a href="elvisex.html#digraph">:digraph</a> command,
<a href="elvisopt.html#digraph">digraph</a> option, and the discussion in
the <a href="elvisinp.html#DIG">Input Mode</a> chapter.
Briefly, the secret is to type <kbd>&lt;Ctrl-K&gt;</kbd> and then the two
ASCII characters that you want to combine,
such as <kbd>&lt;Ctrl-K&gt;&lt;n&gt;&lt;~&gt;</kbd> for "&ntilde;".
<li>You can type <kbd>&lt;Ctrl-X&gt;</kbd> followed by two hex characters to
enter any byte into the edit buffer... provided you know the hex code for
the character you want, of course.
</ul>

<p>If you're having trouble displaying non-ASCII characters, then you may want
to look into the <a href="elvisopt.html#nonascii">nonascii</a> option.
Also, on Unix systems you should verify that your terminal is configured
correctly (8 bits, not 7 -- and the Latin-1 character set is installed).

<dt>Change <!--change use a different--> the default address range for a command
<dd>This is sometimes desirable in an <a href="elvistip.html#ALIAS">alias</a>.
For example the <a href="elvisex.html#write>:w</a> command writes all lines
by default, but <a href="elvisex.html#substitute">:s</a> alters only one line
by default.
You may want to write a macro that uses both of them with the same default,
or just one of them with a different default.

<p>The most straightforward way to do this is to use the
<strong>!{</strong><var>default</var><strong>}%</strong> notation.
Specifically, <strong>!{.}%</strong> will make any command in an alias default
to using just the current line, and <strong>!{%}%</strong> will make any
command default to using all lines.

<p>Here's a simple word-counting alias which uses this technique to count
the words in all lines by default...
<pre>
	alias wcw {
	 local w=0
	 !{%}% s/\w\+/let w=w+1/gx
	 calc w
	}
</pre>

<dt>Execute <!--execute iterate loop--> a particular command separately for each line in a <!--an address--> range <!-- loop looping-->
<dd>The <a href="elvisex.html#global">:g</a> command does this, but only for
the lines which contain a given regular expression.
You can trick it into executing the command for every line by making it look
for something that every line has.
Since every line has a beginning, you can use <strong>:g/^/</strong><var>command</var> to execute the command for every line.

<p>Note that the <a href="elvisex.html#global">:g</a> command can be used with
an address range.
This is often convenient.

<p>Here's an alias which uses this technique to search for the longest line
(assuming all characters are of equal width -- no tabs or control characters)...
<pre>
	alias widest {
	 local w=0 l
	 !%g/^/ {
	  if strlen(line()) &gt; w
	  then let w = strlen(line())
	  then let l = current("line")
	 }
	 calc "Line "l" is the longest, at "w" characters."
	}
</pre>

<dt>Convert <!--convert generate produce derive-->plain text to HTML <!--html--> source <!--input tagged text with tags-->
<dd>Other than the obvious (a long series of manual edit commands),
there are two ways that you can convert text to HTML in elvis.

<p>The most straightforward method is to build an alias which performs a series
of substitutions.
The problem with this method is that it must be rewritten for each type of
input text.
Here's an example of a fairly simple alias that converts plain text to HTML.
(There is a more powerful version of this alias in the standard distribution's
<a href="elvisses.html#elvis.ali">elvis.ali</a> file.)
<pre>
	alias makehtml {
	 "Convert plain text to HTML
	 local report=0
	 "
	 "Protect characters which are special to 
	 try !(%)%s/&amp;/\&amp;amp;/g
	 try !(%)%s/&lt;/\&amp;lt;/g
	 try !(%)%s/&gt;/\&amp;gt;/g
	 "
	 "Convert blank lines to &lt;p&gt; tags
	 try !(%)s/^$/&lt;p&gt;/
	 "
	 "If converting the whole file, then add &lt;html&gt;...&lt;/html&gt;
	 if "!%" == ""
	 then {
	  $a &lt;/body&gt;&lt;/html&gt;
	  1i &lt;html&gt;&lt;body&gt;
	 }
	}
</pre>

<p>The other way is simpler and more versatile, but it requires the use of
an external file.
It uses the <a href="elvisex.html#lpr">:lpr</a> command with
<a href="elvisopt.html#lptype">lptype=html</a>.
Since elvis' print mechanism supports all of elvis' display modes,
you can use this technique to convert any type of text (or even a hex dump
of a binary file) into HTML.

<pre>
	:set lptype=html lplines=0 nolpheader
	:lp foo.html
</pre>

<dt>Add <!--add insert--> new text around highlighted text, via a macro.
<dd>The main trick is to use the visual <a href="elvisvi.html#c">c</a> command
to change the highlighted text, and then use <kbd>^P</kbd> as part of the
replacement text.
While typing the replacement text, <kbd>^P</kbd> causes a copy of the original
text to be inserted.
So, for example, the following macro could be used to and HTML <strong>&lt;strong&gt;</strong>
and <strong>&lt;/strong&gt; </strong>tags around the highlighted text.
<pre>
	:map B c&lt;strong&gt;^P&lt;/strong&gt;^[
</pre>
(Note: When typing that :map command into elvis, you'll need to type
&lt;Ctrl-V&gt;&lt;Ctrl-P&gt; to get the <kbd>^P</kbd>, and
&lt;Ctrl-V&gt;&lt;Esc&gt; to get the <kbd>^[</kbd> character.)
<dd>

</dl>

<h2>C.8 <a name="Contacts">Contacts</a></h2>

<dl>

<dt>Request <!--request get obtain receive-->technical support <!--help questions answers-->
<dd>The best way to get technical support is by posting a question on the
<a href="news:comp.editors">comp.editors</a> newsgroup.
Be sure to mention "elvis" in the subject line, and try to include a succinct
description of the problem there, too.
In the body of the message, be sure to mention the version of elvis you're
using (as reported by "elvis --version"), and your operating system.

<dt>Inform <!--report tell complain inform--> the authors about <!--of--> a bug <!--bugs problems-->
<dd>Bugs should be reported via e-mail.
You may not always receive prompt confirmation, but your bug reports are
appreciated, and acted-upon.
For OS-specific bugs, you should contact the person who ported elvis to your
operating system; the author's names and e-mail addresses are listed in the
<a href="elvisos.html">Operating Systems</a> chapter.

<p>For general bug reports, you should contact the primary author,
Steve Kirkendall,
at <a href="mailto:kirkenda@cs.pdx.edu">kirkenda@cs.pdx.edu</a>.

<p>Either way, be sure to mention the version of elvis you're using
(as reported by "elvis --version"), and your operating system.

<dt>Suggest <!--suggest recommend request ask for add offer give--> a new feature
<dd>Contact the authors as though you were reporting a bug.
See above.

<dt>Get <!--get obtain find--> the latest <!--new newer newest most recent--> version <!--release--> of elvis
<dd>See the "Information via the Web" section of the
<a href="elvistip.html#URLS">Tips</a> chapter.
It has links to various elvis-related sites.

</dl>

</body>
</html>
