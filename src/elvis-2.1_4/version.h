/* version.h */
/* Copyright 1996 by Steve Kirkendall */


#define VERSION	"2.1_4"
#define COPY1 "Copyright (c) 1995-1999 by Steve Kirkendall"
#if 1
# define COPY2 "Permission is granted to redistribute the source or binaries under the"
# define COPY3 "terms of the Perl `Artistic License', as described in the lib/license"
# define COPY4 "file.  In particular, unmodified versions can be freely redistributed."
# define COPY5 "Elvis is offered with no warranty, to the extent permitted by law."
#else
# define COPY2 "This version of elvis is intended to be used only by its developers"
#endif
